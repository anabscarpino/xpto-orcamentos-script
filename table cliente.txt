-- Table: public.cliente

-- DROP TABLE public.cliente;

CREATE TABLE IF NOT EXISTS public.cliente
(
    id integer NOT NULL DEFAULT nextval('cliente_id_seq'::regclass),
    cpf_cnpj character varying(14) COLLATE pg_catalog."default",
    nome character varying(100) COLLATE pg_catalog."default",
    documento_invalido boolean DEFAULT false,
    CONSTRAINT cliente_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cliente
    OWNER to postgres;